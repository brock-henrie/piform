terraform {
  required_providers {
    pihole = {
      source = "ryanwholey/pihole"
    }
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = ">= 1.7.0"
    }
  }

  backend "http" {
  }
}


module "pihole_1_io" {
  source      = "./modules/pihole"
  domain_name = local.domain_name
  domain_ip   = local.domain_ip
  providers = {
    pihole = pihole
  }
}

module "pihole_2_io" {
  source      = "./modules/pihole"
  domain_name = local.domain_name
  domain_ip   = local.domain_ip
  providers = {
    pihole = pihole.secondary
  }
}