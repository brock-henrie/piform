locals {
  pihole_primary_url   = "http://x.x.x.x:8080" # IF you have it on a different port
  pihole_secondary_url = "http://x.x.x.x"      # If you didnt change the port setting
  domain_name          = "demo.mantisd.io"
  domain_ip            = "10.10.1.160"
}