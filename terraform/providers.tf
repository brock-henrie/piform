provider "pihole" {
  url      = local.pihole_primary_url # PIHOLE_URL
  password = var.PIHOLE_PASSWORD      # PIHOLE_PASSWORD
}

provider "pihole" {
  url      = local.pihole_secondary_url # PIHOLE_URL
  password = var.PIHOLE_PASSWORD        # PIHOLE_PASSWORD
  alias    = "secondary"
}