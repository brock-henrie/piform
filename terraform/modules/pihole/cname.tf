resource "pihole_cname_record" "pihole" {
  domain = "pihole.${var.domain_name}"
  target = var.domain_name
}
resource "pihole_cname_record" "pihole-2" {
  domain = "pihole-2.${var.domain_name}"
  target = var.domain_name
}