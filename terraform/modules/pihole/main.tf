terraform {
  required_version = ">=1.1.4"
  required_providers {
    pihole = {
      source = "ryanwholey/pihole"
    }
  }
}


variable "domain_name" {
  type = string
}

variable "domain_ip" {
  type = string
}

data "pihole_cname_records" "cname_records" {}

data "pihole_dns_records" "dns_records" {}


output "dns_records" {
  value = data.pihole_dns_records.dns_records
}

output "cname_record" {
  value = data.pihole_cname_records.cname_records
}