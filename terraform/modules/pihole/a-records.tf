resource "pihole_dns_record" "mantisd-io" {
  domain = var.domain_name
  ip     = var.domain_ip
}