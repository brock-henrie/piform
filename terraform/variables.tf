variable "PIHOLE_PASSWORD" {
  type      = string
  sensitive = true
}